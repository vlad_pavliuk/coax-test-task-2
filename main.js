var departure = {
        x: 7.1,
        y: 5
    },
    destination = {
        x: 6.4,
        y: 3
    },
    _decimalPart = function (number) {
        number = Math.abs(number);
        return number - Math.floor(number)
    },
    validateCoordinates = function (...coordinates) {
        for (var i = 0; i < coordinates.length; i++)
            if (typeof coordinates[i] !== 'number' || coordinates[i] < 0 || coordinates[i] > 10)
                throw('Invalid coordinates');
    },
    perfectCity = function (dep, dest) {
        if (Math.floor(dep['x']) - Math.floor(dest['x']) === 0 && Math.floor(dep['y']) - Math.floor(dest['y']) !== 0) {
            if (Math.abs(_decimalPart(dep['x']) + _decimalPart(dest['x']) > 1)) {
                return 2 - _decimalPart(dep['x']) - _decimalPart(dest['x']) + Math.abs(dep['y'] - dest['y']);
            }
            return _decimalPart(dep['x']) + _decimalPart(dest['x']) + Math.abs(dep['y'] - dest['y']);
        } else if (Math.floor(dep['x']) - Math.floor(dest['x']) !== 0 && Math.floor(dep['y']) - Math.floor(dest['y']) === 0) {
            if (Math.abs(_decimalPart(dep['y']) + _decimalPart(dest['y']) > 1)) {
                return 2 - _decimalPart(dep['y']) - _decimalPart(dest['y']) + Math.abs(dep['x'] - dest['x']);
            }
            return _decimalPart(dep['y']) + _decimalPart(dest['y']) + Math.abs(dep['x'] - dest['x']);
        }
        return Math.abs(dep['x'] - dest['x']) + Math.abs(dep['y'] - dest['y']);
    };

validateCoordinates(departure['x'], departure['y'], destination['x'], destination['y']);
console.log('The shortest distance is: ' + perfectCity(departure, destination));
